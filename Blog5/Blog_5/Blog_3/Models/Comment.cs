﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_3.Models
{
    public class Comment
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống!")]
        [StringLength(int.MaxValue, ErrorMessage = "Nội dung phải có ít nhất 50 ký tự!", MinimumLength = 50)]
        public String Body { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống!")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập đúng kiểu dữ liệu!")]
        public DateTime DateCreated { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống!")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập đúng kiểu dữ liệu!")]
        public DateTime DateUpdated { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống!")]
        public String Author { get; set; }
        public String LastTime
        {
            get
            {
                string temp = "";
                if ((DateTime.Now - DateCreated).Minutes >= 1 && (DateTime.Now - DateCreated).Hours < 1)
                    temp = (DateTime.Now - DateCreated).Minutes.ToString() + " phút" + " " + (DateTime.Now - DateCreated).Seconds.ToString() + " giây";
                else if ((DateTime.Now - DateCreated).Hours >= 1)
                {
                    temp = (DateTime.Now - DateCreated).Hours.ToString() + " giờ" + " " + (DateTime.Now - DateCreated).Minutes.ToString() + " phút" + " " + (DateTime.Now - DateCreated).Seconds.ToString() + " giây";
                }
                else
                {
                    temp = (DateTime.Now - DateCreated).Seconds.ToString() + " giây";
                }
                return temp;
            }
        }

        public int PostID { get; set; }
        public virtual Post Post { get; set; }
    }
}