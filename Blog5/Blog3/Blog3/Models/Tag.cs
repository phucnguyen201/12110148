﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog3.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required]
        public string Content { set; get; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}