﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog3.Models
{
    public class Post
    {
       
        
        public int ID { set; get; }
        [Required]
        [MaxLength(500, ErrorMessage="Độ dài tối đa là 500 ký tự")]
        [MinLength(20, ErrorMessage = "Độ dài tối thiểu là 20 ký tự")]
        public String Title { set; get; }
        [Required]
        [MinLength(20, ErrorMessage="Độ dài tối thiểu là 50 ký tự")]
        public String Body { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }

        //Tạo quan hệ 1-n. 1 account tương ứng với 1 post
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserID { set; get; }

        //Tạo quan hệ 1- post có nhiều comment
      
        
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}