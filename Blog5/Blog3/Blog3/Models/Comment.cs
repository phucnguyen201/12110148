﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog3.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required]
        [MinLength(20, ErrorMessage = "Độ dài nhỏ nhất là 20 ký tự")]
        public String Body { set; get; }
        [Required]
        public DateTime DateCreated { set; get; }
        [Required]
        public DateTime DateUpdated { set; get; }
        [Required]
        public String Author { set; get; }
        public int Lastime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }


        public int PostID { set; get; }
        public virtual Post Post { set; get; }
        
    }
}