﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Comment
    {

        public int ID { set; get; }
        [StringLength(100, ErrorMessage = "Không nhập quá 500 kí tự", MinimumLength = 0)]
        public String Body { set; get; }
        [DataType(DataType.DateTime, ErrorMessage = "Vui lòng nhập đúng định dạnh ngày mm/dd/yyyy")]
        public DateTime DateCreated { set; get; }
        [DataType(DataType.DateTime, ErrorMessage = "Vui lòng nhập đúng định dạnh ngày mm/dd/yyyy")]
        public DateTime DateUpdated { set; get; }
        [Required]
        public String Author { set; get; }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}