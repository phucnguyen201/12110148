﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [StringLength(100, ErrorMessage = "Không nhập quá 100 kí tự", MinimumLength = 0)]
        public String Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}