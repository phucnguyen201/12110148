﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Account
    {

        public int AccountID { set; get; }
        [StringLength(100, ErrorMessage = "Không nhập quá 100 kí tự", MinimumLength=0)]
        public String FirstName { set; get; }
        [StringLength(100, ErrorMessage = "Không nhập quá 100 kí tự", MinimumLength = 0)]
        public String LastName { set; get; }
        [DataType(DataType.Password,ErrorMessage="Mật khẩu không hợp lệ")]
        public String Password { set; get; }
        [DataType(DataType.EmailAddress,ErrorMessage="Email không hợp lệ")]
        public String Email { set; get; }

        public virtual ICollection<Post> Posts { set; get; }

    }
}