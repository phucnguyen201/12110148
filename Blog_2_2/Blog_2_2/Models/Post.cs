﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    [Table("Post")]
    public class Post
    {
        public int ID { set; get; }
        [StringLength(100, ErrorMessage = "Không nhập quá 100 kí tự", MinimumLength = 0)]
        public String Title { set; get; }
        [StringLength(500, ErrorMessage = "Không nhập quá 500 kí tự", MinimumLength = 0)]
        public String Body { set; get; }
        [DataType(DataType.DateTime, ErrorMessage = "Vui lòng nhập đúng định dạnh ngày mm/dd/yyyy")]
        public DateTime DateCreated { set; get; }
        [DataType(DataType.DateTime, ErrorMessage = "Vui lòng nhập đúng định dạnh ngày mm/dd/yyyy")]
        public DateTime DateUpdated { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public int AcountID { set; get; }
        public virtual Account Account { set; get; }
    }
}