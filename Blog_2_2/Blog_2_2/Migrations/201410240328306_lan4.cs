namespace Blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.AccountID);
            
            AddColumn("dbo.Post", "AcountID", c => c.Int(nullable: false));
            AddColumn("dbo.Post", "Account_AccountID", c => c.Int());
            AddForeignKey("dbo.Post", "Account_AccountID", "dbo.Accounts", "AccountID");
            CreateIndex("dbo.Post", "Account_AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Post", new[] { "Account_AccountID" });
            DropForeignKey("dbo.Post", "Account_AccountID", "dbo.Accounts");
            DropColumn("dbo.Post", "Account_AccountID");
            DropColumn("dbo.Post", "AcountID");
            DropTable("dbo.Accounts");
        }
    }
}
