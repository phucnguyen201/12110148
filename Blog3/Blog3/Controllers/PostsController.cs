﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog3.Models;

namespace Blog3.Controllers
{
    [Authorize]
    public class PostsController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Posts/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.UserProfile);
            return View(posts.ToList());
        }

        //
        // GET: /Posts/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            //Để truyền dữ liệu vào parital View _addComment
            ViewData["idpost"] = id;
            //Lưu giữ session người dùng
            Session["Post"] = post;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Posts/Create
        // Tag 
        public ActionResult ViewPostInTag(int id=0)
        {
            Tag tag = db.Tags.Find(id);
            return View(tag);
        }
        public ActionResult Create()
        {
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Posts/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post, string Content)
        {
            if (ModelState.IsValid)
            {
                post.DateCreated = DateTime.Now;
                post.DateUpdated = DateTime.Now;
                //Tạo DS các Tags
                List<Tag> Tags = new List<Tag>();
                //Tách các tag theo dấu
                string[] TagContent = Content.Split(',');
                //Lập danh sách các tag vừa tách
                foreach (string item in TagContent)
                {
                    //Tìm xem tag content đã có hay chưa
                    Tag tagExist = null;
                    var listTag = db.Tags.Where(y => y.Content.Equals(item));
                    int Count = listTag.Count();
                    if (Count > 0)
                    {
                        //Nếu có tag comment rồi thì add thêm post vào
                        tagExist = listTag.First();
                        tagExist.Posts.Add(post);
                    }
                    else
                    {
                        //Nếu chưa có tag comment thì tạo tag và add post vào
                        tagExist = new Tag();
                        tagExist.Content = item;
                        tagExist.Posts = new List<Post>();
                        tagExist.Posts.Add(post);
                    }
                    //Add tagExist vào trong Tag
                    Tags.Add(tagExist);
                }
                //Add Tag vào Post
                post.Tags = Tags;
                //Lấy userid;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                post.UserProfileUserID = userid;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserID);
            return View(post);
        }

        //
        // GET: /Posts/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserID);
            return View(post);
        }

        //
        // POST: /Posts/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                post.DateUpdated = DateTime.Now;
                post.DateCreated = db.Posts.Where(x => x.ID == post.ID).Select(x => x.DateCreated).Single();
                post.UserProfileUserID = db.Posts.Where(x => x.ID == post.ID).Select(x => x.UserProfileUserID).Single();
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserID);
            return View(post);
        }

        //
        // GET: /Posts/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Posts/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}