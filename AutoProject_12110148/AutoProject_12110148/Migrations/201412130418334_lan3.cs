namespace AutoProject_12110148.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Profiles", "DiemTichLuy", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Profiles", "DiemTichLuy");
        }
    }
}
