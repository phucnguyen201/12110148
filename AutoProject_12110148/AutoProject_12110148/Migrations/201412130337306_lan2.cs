namespace AutoProject_12110148.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Profiles", "NgaySinh", c => c.DateTime(nullable: false));
            DropColumn("dbo.Profiles", "Password");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Profiles", "Password", c => c.String());
            DropColumn("dbo.Profiles", "NgaySinh");
        }
    }
}
