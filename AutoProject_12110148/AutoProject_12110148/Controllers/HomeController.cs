﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoProject_12110148.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcom to AutoPro.Net !";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Website tổng hợp tin tức Ôtô - Xe máy";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Liên hệ.";

            return View();
        }
    }
}
