﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoProject_12110148.Models;

namespace AutoProject_12110148.Controllers
{
    [Authorize]
    public class PostController : Controller
    {

        private AutoProDbContext db = new AutoProDbContext();

        //
        // GET: /Post/

        [AllowAnonymous]
        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.Profile).Include(p => p.Category);
            return View(posts.ToList());
        }
        [AllowAnonymous]
        public ActionResult IndexOto()
        {
            var posts = db.Posts.Where(x => x.CategoryID == 1).Include(p => p.Profile).Include(p => p.Category);
            return View(posts.ToList());
        }
        [AllowAnonymous]
        public ActionResult IndexMotoXemay()
        {
            var posts = db.Posts.Where(x => x.CategoryID == 2).Include(p => p.Profile).Include(p => p.Category);
            return View(posts.ToList());
        }
        [AllowAnonymous]
        public ActionResult IndexDanhGia()
        {
            var posts = db.Posts.Where(x => x.CategoryID == 3).Include(p => p.Profile).Include(p => p.Category);
            return View(posts.ToList());
        }
        [AllowAnonymous]
        public ActionResult IndexKyThuat()
        {
            var posts = db.Posts.Where(x => x.CategoryID == 4).Include(p => p.Profile).Include(p => p.Category);
            return View(posts.ToList());
        }

        //
        // GET: /Post/Details/5

        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            int user = 3;
            Post post = db.Posts.Find(id);
            ViewBag.Rating = post.RatingPost;
            ViewData["idpost"] = id;
            if (Request.IsAuthenticated)
            {
                user = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                          .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                ViewData["iduser"] = user;
            }
            else
                ViewData["iduser"] = 3;
            if (user != 3) //Khác Anonymous
            {
                string chuoiRating = "Đánh giá của bạn: ";
                if (db.Ratings.Count(p => p.PostID == id && p.UserId == user) > 0)
                {
                    int ra = db.Ratings.Where(p => p.PostID == id && p.UserId == user).Select(r => r.RatingValue).Single();
                    if (ra == 1)
                        chuoiRating += "<b><a href=/Post/Rating?post=" + id + "&value=1&ratingnew=0> 1 </a></b>";
                    else
                        chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=1&ratingnew=0> 1 </a>";
                    if (ra == 2)
                        chuoiRating += "<b><a href=/Post/Rating?post=" + id + "&value=2&ratingnew=0> 2 </a></b>";
                    else
                        chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=2&ratingnew=0> 2 </a>";
                    if (ra == 3)
                        chuoiRating += "<b><a href=/Post/Rating?post=" + id + "&value=3&ratingnew=0> 3 </a></b>";
                    else
                        chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=3&ratingnew=0> 3 </a>";
                    if (ra == 4)
                        chuoiRating += "<b><a href=/Post/Rating?post=" + id + "&value=4&ratingnew=0> 4 </a></b>";
                    else
                        chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=4&ratingnew=0> 4 </a>";
                    if (ra == 5)
                        chuoiRating += "<b><a href=/Post/Rating?post=" + id + "&value=5&ratingnew=0> 5 </a></b>";
                    else
                        chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=5&ratingnew=0> 5 </a>";
                }
                else
                {
                    chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=1&ratingnew=1> 1 </a>";
                    chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=2&ratingnew=1> 2 </a>";
                    chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=3&ratingnew=1> 3 </a>";
                    chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=4&ratingnew=1> 4 </a>";
                    chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=5&ratingnew=1> 5 </a>";

                }
                ViewBag.YourRating = chuoiRating + " sao";
                
            }
            if (post == null)
            {
                return HttpNotFound();
            }
            
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Profiles, "UserId", "Name");
            ViewBag.CategoryID = new SelectList(db.Categorys, "ID", "Name");
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post)
        {
            if (ModelState.IsValid)
            {
                post.DateCreated = DateTime.Now;
                post.DateUpdated = DateTime.Now;
                post.UserId = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                        .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                post.TongSoRate = 0;
                post.TongUserRate = 0;

                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.Profiles, "UserId", "Name", post.UserId);
            ViewBag.CategoryID = new SelectList(db.Categorys, "ID", "Name", post.CategoryID);
            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.Profiles, "UserId", "Name", post.UserId);
            ViewBag.CategoryID = new SelectList(db.Categorys, "ID", "Name", post.CategoryID);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.Profiles, "UserId", "Name", post.UserId);
            ViewBag.CategoryID = new SelectList(db.Categorys, "ID", "Name", post.CategoryID);
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult Rating(int post, int value, int ratingnew)
        {
            Rating ra = new Rating();
            Post po = new Post();
            po = db.Posts.Where(p => p.ID == post).Single();
            int user = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                        .Where(y => y.UserName == User.Identity.Name).Single().UserId;
            if (ratingnew == 1)
            {
                ra.PostID = post;
                ra.UserId = user;
                ra.RatingValue = value;
                db.Ratings.Add(ra);
                po.TongSoRate = po.TongSoRate + value;
                po.TongUserRate = po.TongUserRate + 1;
                db.SaveChanges();
            }
            else
            {
                ra = db.Ratings.Where(p => p.PostID == post && p.UserId == user).Single();
                ra.RatingValue = value;
                po.TongSoRate = po.TongSoRate - db.Ratings.Where(p => p.PostID == post && p.UserId == user).Select(r => r.RatingValue).Single(); //giá trị cũ
                po.TongSoRate = po.TongSoRate + value;
                db.SaveChanges();
            }
            return RedirectToAction("Details/" + post, "Post");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}