﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AutoProject_12110148.Models
{
    public class Profile
    {
        [System.ComponentModel.DisplayName("Tên Tác Giả")]
        public String Name { get; set; }
        public String Email { get; set; }
        public bool GioiTinh { get; set; }
        public DateTime NgaySinh { get; set; }
        public int DiemTichLuy { get; set; }
        public DateTime NgayTaoTK { get; set; }
        public String DiaChi { get; set; }
        //Lien ket 1 - 1 voi Account

        [Key]
        [ForeignKey("UserProfile")]
        public int UserId { get; set; }

        //Moi Profile thuoc 1 User
        public virtual UserProfile UserProfile { get; set; }

        //Moi Profile User thuoc 1 Level
        public int LevelID { get; set; }
        public virtual Level Level { get; set; }

        //Profile co nhieu Post va Comment / Rate
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
    }
}