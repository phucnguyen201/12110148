﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoProject_12110148.Models
{
    public class Level
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public int Diem { get; set; }
        public virtual ICollection<Profile> Profiles { get; set; }
    }
}