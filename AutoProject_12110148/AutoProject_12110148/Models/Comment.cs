﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AutoProject_12110148.Models
{
    public class Comment
    {

        public int ID { get; set; }

        [Required(ErrorMessage = "Nội dung không được bỏ trống!")]
        [StringLength(int.MaxValue, ErrorMessage = "Nội dung phải có ít nhất 50 ký tự!", MinimumLength = 50)]
        public String Body { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { get; set; }

        //Moi comment thuoc 1 post
        public int PostID { get; set; }
        public virtual Post Post { get; set; }

        //1 Account co nhieu comment
        [ForeignKey("Profile")]
        public int UserId { get; set; }
        public virtual Profile Profile { get; set; }

        public String LastTime
        {
            get
            {
                string temp = "";
                if ((DateTime.Now - DateCreated).Minutes >= 1 && (DateTime.Now - DateCreated).Hours < 1)
                    temp = (DateTime.Now - DateCreated).Minutes.ToString() + " phút" + " " + (DateTime.Now - DateCreated).Seconds.ToString() + " giây";
                else if ((DateTime.Now - DateCreated).Hours >= 1)
                {
                    temp = (DateTime.Now - DateCreated).Hours.ToString() + " giờ" + " " + (DateTime.Now - DateCreated).Minutes.ToString() + " phút" + " " + (DateTime.Now - DateCreated).Seconds.ToString() + " giây";
                }
                else
                {
                    temp = (DateTime.Now - DateCreated).Seconds.ToString() + " giây";
                }
                return temp;
            }
        }
    }
}