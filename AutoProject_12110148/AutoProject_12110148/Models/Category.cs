﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoProject_12110148.Models
{
    public class Category
    {
        public int ID { set; get; }
        [System.ComponentModel.DisplayName("Tên Danh Mục")]
        public String Name { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}