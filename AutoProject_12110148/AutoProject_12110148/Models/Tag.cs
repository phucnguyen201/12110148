﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoProject_12110148.Models
{
    public class Tag
    {
        public int ID { get; set; }
        public String Body { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}