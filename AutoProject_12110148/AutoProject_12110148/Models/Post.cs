﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AutoProject_12110148.Models
{
    public class Post
    {
        public int ID { set; get; }


        [Required(ErrorMessage = "Tiêu đề không được bỏ trống !")]
        [MaxLength(500, ErrorMessage = "Độ dài tối đa là 500 ký tự")]
        [MinLength(20, ErrorMessage = "Độ dài tối thiểu là 20 ký tự")]
        [System.ComponentModel.DisplayName("Tiêu đề")]
        public String Title { set; get; }


        [Required(ErrorMessage = "Nội dung không được bỏ trống !")]
        [MinLength(20, ErrorMessage = "Độ dài tối thiểu là 50 ký tự")]
        [System.ComponentModel.DisplayName("Nội dung")]
        public String Body { set; get; }


        [Required]
        [DataType(DataType.DateTime)]
        [System.ComponentModel.DisplayName("Ngày tạo")]
        public DateTime DateCreated { set; get; }


        [Required]
        [DataType(DataType.DateTime)]
        [System.ComponentModel.DisplayName("Ngày cập nhật")]
        public DateTime DateUpdated { set; get; }

        [System.ComponentModel.DisplayName("Giá Trị Đánh Giá")]
        public int TongSoRate { get; set; }
        [System.ComponentModel.DisplayName("Số Người Đánh Giá")]
        public int TongUserRate { get; set; }

        //Lay so rate ( 1 2 3 4 5 sao )
        [System.ComponentModel.DisplayName("Đánh Giá Bài Viết")]
        public float RatingPost
        {
            get
            {
                if (TongUserRate != 0)
                    return TongSoRate / TongUserRate;
                else
                    return 0;
            }
        }

        [System.ComponentModel.DisplayName("Tên Tác Giả")]
        //Tạo quan hệ 1-n. 1 Profile tương ứng với 1 post
        [ForeignKey("Profile")]
        public int UserId { get; set; }
        public virtual Profile Profile { get; set; }

        //Tạo quan hệ 1- post có nhiều comment / Tag /Rate
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual ICollection<Rating> Ratings { get; set; }

        //Tao quan he 1 post thuoc 1 category
        public int CategoryID { get; set; }
        public virtual Category Category { get; set; }
    }
}