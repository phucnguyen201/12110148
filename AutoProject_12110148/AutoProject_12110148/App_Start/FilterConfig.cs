﻿using System.Web;
using System.Web.Mvc;

namespace AutoProject_12110148
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}